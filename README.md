## Created by Serena Sanna

## VERSION Dec 2017 

## for more info write me at aamichigan@gmail.com or s.sanna@umcg.nl 

## DESCRIPTION: 
BreakLargePed.pl breaks a large family in two subfamilies, by duplicating one sample and pretending the duplicate is not related to his/her parents. 
The sample to be duplicated is chosen such that the subfamilies are as large as possible. 
If the subfamilies are still too large commpared to what requested, the algorithm is repeated automatically. 
It can handle a pedigree file with multiple large and small families together.  


## REQUIREMENTS
BreakLargePed.pl It's a Perl script, so you should have Perl installed on your machine.

The script interacts with PEDSTATS, so, you need to have PEDSTATS installed on your machine. 
You can download it from here:
http://csg.sph.umich.edu/abecasis/pedstats/download/

## INPUT FILES

The input files are  a pedfile to describe your pedigree, and the dat file (on QTDT/Merlin format, 
http://www.sph.umich.edu/csg/abecasis/Merlin/tour/input_files.html )


In the pedfile, the first 5 columns are mandatory. One extra variable in your dat file is required to indicate who has not informative data:
T phenotype_data    --> with values  x for missing, 2 for present 

for example, if you are doing a genetic analysis it is important to say which samples have missing genotypes. 

NOTE: individuals must have unique IDs in your pedfile, even across different families


## OPTIONS 


You can specify your ped and dat file as:
perl BreakLargePed.pl  -p your_pedfile -d your_datafile

Family size is evaluated by the number of bits required to store the data. 
The default cutoff  is bits=20, but you can change it using the option: 
-b your_cutoff.

If you request -b 20, all families will be broken in subfamilies *SMALLER* than 20 bits.

You can specify the path for pedstats using the option:
-location pedstats_path (example: /home/programs/pedstats)



## OUTPUT FILES
While  it's running, you should view on your screen which family the 
script is breaking, the number of bits, which sample has been selected for being duplicated 
and the bit of subfamilies t

Duplicated individual is marked with "b" on your output ped file

## CITATION:

if you use this program for publication, please cite: 
Scuteri, Sanna et al Plos Gen 2007 
https://www.ncbi.nlm.nih.gov/pubmed/17658951

and you can sponsor also the bitbucket code :)

THANK YOU!

### TRY IT OUT!!

you can use example.ped and example.dat to have a sense of file formatting and how the script works.
Don't forget to install pedstats! have fun!

