#!/usr/bin/perl 

#### break large pedigrees in smaller ones given a bits number cut-off
# method: duplicate the individual who generates the two largest subfamilies
#	-p pedigree file
#	-d datfile
#	-b bits cutoff
#
# Dec 2009 - Serena Sanna
##
# Changes compared to the previous version:
#  only one column need to be supplied, with the informativity for each individual
#  No additional columns can be supplied
#  Report now prints detailed counts for each bit
#  Output on screen now prints bits for families even if they don't need to be broken
#
#

print "\n"."#==========================#\n".
    " BreakLargePed.pl : Dec 2017 \n Serena Sanna\n".
    'aamichigan@gmail.com ; s.sanna@umcg.nl '."\n".
    "\n#=========================#\n\n";


use strict;
use Getopt::Std;

my $bits_cutoff=20;
my %opts=();
my $program='pedstats';

getopts('p:d:b:o:l:',\%opts);
if ( !defined $opts{p} || !defined $opts{d}){print "\n". "FATAL ERROR: \n\n ####### \n please specify  files names using: \n"."\n".
			 "  -p your_ped_file -d your_dat_file  "."\n"." ######\n\n".
			 "\n" ;exit};


print "\n>>>>> this script will break large families in smaller ones of less than  $bits_cutoff  bits  <<<<< \n\n";
print "##\n"."Your 6th column need to be the informativity column. Entries have to be x (non informative) and 2 (informative)\n";
print "This colum should appear as A INFORMATIVITY in the dat file\n\n";
print "Please do NOT supply additional columns\n";

print "\n Starting process.....\n\n";

if (!defined $opts{b}) { print  "\nNOTES: bits_cutoff_default=20. To change it, use the option:  -b bits_cutoff"."\n\n" ;
  }else{  $bits_cutoff=$opts{b}; }



my $root_out="yourfile" ; 
if (defined $opts{o}) { $root_out=$opts{o}; }

my $inputfile=$opts{p};
my $datfile=$opts{d};
my $original_datfile=$datfile;
my $infocolumn=5; # 6th column in the ped file

if (defined $opts{l}) {$program=$opts{l};print "pedstats found at $program\n\n"}



my $loop=1;
my %status=();
my %bits_fam=();
my %f;
my %d;
my %family=(); 	## family ==> list individual ==> corrispondent data
my %data=(); 	 ## corrispondent data of each individual
my @fam=();
my %original_data;
my %informative;
my %spouse;
open (D,"<$datfile") || die "impossible to open data file $datfile";
my @dati;
while (my$line=<D>){
    chomp ($line);
    my @v= split(" ",$line);
    push @dati, $v[1];
    if ($v[0] eq ' '){shift @v};
    if ($v[0] ne "E"){
	push @dati, $v[1];
	if ($v[0] eq "M"){push @dati, $v[1];} ## consider markers as double column
    };
    
}


close(D);
if(-e "$root_out.broken_ped"){system "rm $root_out.broken_ped";}
if(-e "$root_out.report"){system "rm $root_out.report";}
if(-e "to_repeat.ped"){system "rm to_repeat.ped";}
open (OUT, ">> $root_out.broken_ped") || die " Impossible to write $root_out.broken_ped output";
open (OUTREP,">> $root_out.report ") || die "Impossible to write $root_out.report file ";

my $count_run=1;
print OUTREP  "\n"."\n"."#### 1st run ####"."\n"."\n"."\n";

print OUTREP "FAM\tIND_DUPL\tbits_FAM1\tCONFIG\tbits_FAM2\tCONFIG"."\n";
while($loop>0){
    if($inputfile ne "to_repeat.ped"){$loop=0;}

    if ($loop >0){ print "\n"."restarting to break..."."\n"."\n"; $loop=0;}
    open(IN, "< $inputfile") ||  die "impossible to open input file $inputfile";


    while( my $line=<IN>){
	if($line eq ""){next};
	chomp ($line);
	$line =~ s/0\/0/x x/g;  ## convert genotype 0/0 -> 0 0
	$line =~ s/\// /g; ## convert genotype 1/1 -> 1 1
	my @v=split(" ",$line);
	if ($v[0] eq " ") {shift @v};
	push @fam, $v[0]; 
	$v[1] =~ s/ //g;
	push @{$family{$v[0]}}, $v[1]; 
	push @{$data{$v[1]}}, $v[2]; #father
	push @{$data{$v[1]}}, $v[3]; #mother
	push @{$data{$v[1]}}, $v[4]; #sex
	$spouse{$v[2]}=$v[3];
	$spouse{$v[3]}=$v[2];
	if ($#dati <0){$informative{$v[1]}="yes";};  ## if pedfile doesn't contain information, treat all individuals as informatives

	foreach my $i (5..$#v){  
	    push @{$data{$v[1]}}, $v[$i];	
	    if($inputfile ne "to_repeat.ped"){push @{$original_data{$v[1]}}, $v[$i]; 
	     		      
	    } 
	}
    }

    @dati=();
    if ($inputfile eq "to_repeat.ped"){ system "rm to_repeat.ped";}
    foreach my $k (@fam){

	if(! defined $bits_fam{$k}){
	    %f=%family; ## array to use for calculate bits
	    %d=%data;  ## array to use
	    @{$bits_fam{$k}}=&bits($k);
	    print  "Family: $k $bits_fam{$k}[0], descendants: $bits_fam{$k}[1], founders: $bits_fam{$k}[2], couple simmetry: $bits_fam{$k}[3] \n";
	    
	    if ($bits_fam{$k}[0]  < $bits_cutoff) { 
                  print "No need to split Family $k, bits $bits_fam{$k}[0]\n";
		foreach my $i ( @{$family{$k}} ){
		    if ($status{$i} ne "TRIM"){ 
			print OUT " $k $i ";
			foreach my $d (0..2){
			    
			    print OUT  " ${$data{$i}}[$d] "; }
		   	my $IND=$i;
			$IND =~ s/b//g;
			foreach my $d (0..$#{$original_data{$IND}}){
			    
			    print OUT  " ${$original_data{$IND}}[$d] "; }
			print OUT "\n";
		    }
		    
		    
		}
		
		
	    }else{
		
		print "\n"." Breaking family $k  bits: $bits_fam{$k}[0] <> descendents : $bits_fam{$k}[1] founders: $bits_fam{$k}[2]"."\n";
		print OUTREP "  ============ "."\n";
		my @to_duplicate=();
		my %bitsINFO=();
		foreach my $i (@{$family{$k}}){
		    my $flag1=0; ## flag controlling duplicate
		    my $flag2=0; # flag controlling descendents at the bottom who will create singletons
		    if ( ($status{$i} eq "n") && ($i ne "")){
			foreach my $j (@{$family{$k}}){
			    my $bis=$i."b"; 
			    if($j =~ $bis){$flag1++;}  ## have been duplicated yet?
			    if( ($d{$j}[0] eq $i) || ($d{$j}[1] eq $i )){$flag2++;} ## if $i is the father or mother of someone 
			}
			if (($flag1==0) && ($flag2>0)){push @to_duplicate, $i;} } ## duplicate only non-founders, not yet duplicate, and with at least a child
		    
		}		
		if ($#to_duplicate == -1) {print "\nWARNING no descendents to duplicate\n";print "\nfamily $k wrote w/out breaking \n";
					   foreach my $i (@{$family{$k}}){
					       print OUT "$k $i ";
					       foreach my $z (@{$data{$i}}){
						   print OUT $z." ";}
					       $i=~s/b//g;
					       foreach my $z (@{$original_data{$i}}){
						   print OUT $z." ";
					       }
					       print OUT "\n";
					   }
					   next;
		}
		foreach my $dup (@to_duplicate){
		    
		    foreach my $i (@{$family{$k}}){
			if($status{$i} ne "TRIM"){undef $status{$i};}
		    }
		    open(OUTA, "> for_pedstats.ped") || die "Impossible to open for_pedstats.ped";
		    &duplica($dup, $k);
		    close(OUTA);
		    
		    system "$program -p for_pedstats.ped -d $datfile --rewrite --ignore> pedstats.out ";
		    undef %f;
		    undef %d;
		    my @r=&readfile("pedstats.ped", $dup);
		    if ($#r >1){print "Warning: duplicating individual $dup \n".
				    "family $k splitted in more than two: $r[0] $r[1] $r[2]"."\n";};
		    
		    @{$bits_fam{$r[0]}}=&bits($r[0]);
		    
		    
		    @{$bits_fam{$r[1]}}=&bits($r[1]);
		    
		    my $diff=&diff_absol($bits_fam{$r[0]}[0] , $bits_fam{$r[1]}[0]) ;
		    
		    print OUTREP "$k\t$dup\t".
			"$bits_fam{$r[0]}[0]\t(N-${$bits_fam{$r[0]}}[1],F-${$bits_fam{$r[0]}}[2],C-${$bits_fam{$r[0]}}[3])\t".
			"$bits_fam{$r[1]}[0]\t(N-${$bits_fam{$r[1]}}[1],F-${$bits_fam{$r[1]}}[2],C-${$bits_fam{$r[1]}}[3])".
			"\n";
		    push @{$bitsINFO{ $diff}}, ${$bits_fam{$r[0]}}[0];  ## bits family 1
		    push @{$bitsINFO{ $diff}}, ${$bits_fam{$r[1]}}[0]; ## bits family 2
		    push @{$bitsINFO{ $diff}}, $dup; ### individual duplicated
		    push @{$bitsINFO{ $diff}}, $r[0];  ## familiy 1 generated
		    push @{$bitsINFO{ $diff}}, $r[1];  ##family2 generated
		    if (${$bits_fam{$r[0]}}[0] + ${$bits_fam{$r[1]}}[0] == 0){
			print "trouble calculing bits: all families have zero bits"."\n";}
		    
		    #  $bitsINFO{ $diff} = @info; ## diff ==> bits1 , bits2 , indiv_dupl, fam1, fam2
		    system "rm for_pedstats.ped";
		    system "rm pedstats.ped";	
		    
		}
		
		my @w= sort {$a <=> $b} keys %bitsINFO;
		
		open(OUTA, "> for_pedstats.ped") || die "Impossible to open for_pedstats.ped";
		print " duplicating individual ${$bitsINFO{$w[0]}}[2] "."\n";
		&duplica(${$bitsINFO{$w[0]}}[2] , $k) ;
	        print OUTREP " duplicating individual ${$bitsINFO{$w[0]}}[2] is optimal"."\n";	
		close(OUTA);
		system "$program -p for_pedstats.ped -d $datfile --rewrite --ignore> pedstats.out ";
		open (WARN, "grep WARN pedstats.out |") || die "can't read pedstats.out";
		my $warning=<WARN>;
		if ($warning ne ""){print "\nplease check pedstats.out\n";exit;}
		close(WARN);
		if( ($bitsINFO{$w[0]}[0] <$bits_cutoff) and ($bitsINFO{$w[0]}[1] <$bits_cutoff) ){

		    &writefile ("pedstats.ped","$root_out.broken_ped");
		    print " => splitted in fam $bitsINFO{$w[0]}[3], bits $bitsINFO{$w[0]}[0] ".
			"and fam $bitsINFO{$w[0]}[4], bits $bitsINFO{$w[0]}[1] "
		        ."\n";
		    system "rm pedstats.out";
		    system "rm pedstats.ped";
		    system "rm for_pedstats.ped";
		}else{
		    
		    &writefile ("pedstats.ped","to_repeat.ped");
		    print "Need to break again family $k, bits $bitsINFO{$w[0]}[0] and  $bitsINFO{$w[0]}[1] "."\n";
		    $inputfile="to_repeat.ped";
		    $loop++;
		}
		print "############"."\n";
		%bitsINFO=();
	    }	
	}

    }
    
    close (IN);

    if($loop ==0){print "\n"."new pedigree saved on $root_out.broken_ped,  info in report.txt"."\n";
		  if($datfile ne $original_datfile){system "rm $datfile"};
		  
    }else{
	$count_run++;
	$inputfile="to_repeat.ped"; #$datfile="for_pedstats.dat";
	%family=(); ## family ==> list individual ==> corrispondent data
	%data=();  ## corrispondent data of each individual
	@fam=();
	%bits_fam=();
	%status=();
	print "\n"."please wait, need to break again some family"."\n";
	print OUTREP  "\n"."\n"."#### $count_run"."th run ####"."\n"."\n"."\n";
    }
    

    
    
    
}
close(OUT);
system "sed -e's/  / /g' -e's/^ //g' $root_out.broken_ped >A";
system "mv A $root_out.broken_ped";

########################################################################################################################################

####  count bits for a family  ### arguments:  family ,
sub bits {  
    
    my $f=0;
    my $n=0;
    my $c=0;
    my $a=$_[0];  ## family number 
    foreach my $i (@{$f{$a}}){  ## foreach individual in this family
	
	if (! defined $status{$i}){ ## if status is undef
	   
                  
	    if ( ($d{$i}[0] eq 0) && ($d{$i}[1] eq 0 )){  
		$status{$i}="f"; $f++;  ## if parents are 0 0, set individual as founder
	    } else{
		my $flag=0;  
		foreach my $j (@{$f{$a}}){
		    ### look for sibs or half sib
		    if ($j ne $i){
			if( ($d{$j}[0] eq  $d{$i}[0]) || ($d{$j}[1] eq $d{$i}[1]) ){
			    if (! defined $status{$i}){$status{$i}="n", $n++;}
			    if (! defined $status{$j}){$status{$j}="n", $n++;}
			    $flag++;
			}
		    }
		} ## set flag >0 if there is any sib
		
		### look for founder couple simmetry#####  
		my $DAD=$d{$i}[0]; ##father
		$DAD=~s/b//g;
		my $MAM=$d{$i}[1]; ##mother
		$MAM=~s/b//g;	
		# since there are sibs, check if parents are not informative to use the simmetry
               if ( ($flag >0 )&& ( ( $d{$DAD}[$infocolumn-2] eq 'x' )  && ($d{$MAM}[$infocolumn-2] eq 'x' ))
                     && ($d{ $DAD }[0] eq 0) && ($d{ $MAM }[0] eq 0)  ) {	
		
		    if (!defined  $status{$DAD} ) {
			$status{$DAD}="f";
			$f++;
		    }
		    
		    if (!defined  $status{$MAM} ) {
			$status{$MAM}="f";
			$f++;
			
		    }
		    $c ++;
		
		}
		
	#print"info for padre di $DAD at column $infocolumn-2 is  $d{$DAD}[$infocolumn-2] \n";
		### since there are not sibs, control if it is possible to trim the parents
		if ( ($flag ==0) && ( ( $d{$DAD}[$infocolumn-2] eq 'x' )  && ( $d{$MAM}[$infocolumn-2] eq 'x')) && ($d{$d{$i}[0]}[0] eq 0) && ($d{$d{$i}[1]}[0] eq 0) ) {
		    if (  $status{$d{$i}[0]} eq "f" ){ 
			$status{$d{$i}[0]}="TRIM" ; 
			$f--; 
		    }else{
			$status{$d{$i}[0]}="TRIM";
		    }
		    if ($status{$d{$i}[1]} eq "f" ){
			$status{$d{$i}[1]}="TRIM" ;
			$f--;
		    }else{
			$status{$d{$i}[1]}="TRIM";
		    }
		    if(%d == %data){
			$d{$i}[0]=0; $d{$i}[1]=0;  }## set fath == 0 && moth == 0
		    
		    $status{$i}="f"; $f++; 
	        
		}elsif($flag == 0) {
		    $status{$i}="n", $n++; ## no sibs but informatives parents
		}	
	    }

	}
	
    }

    my @a=();
    #print "$n $f $c \n";
    my $b=2*$n - $f -$c;
    if($b <0){$b=0;}
    push @a, $b;
    push @a, $n;
    push @a, $f;
    push @a , $c;
    return   @a;

}

######################################		

### duplicate one individual arguments ( individual , family,)
sub duplica {  
 
     print OUTA "$_[1] $_[0]b 0 0 $data{$_[0]}[2]"; 
		 ## then attach original phenotype and genotype
		  my $indb=$_[0];
		    $indb=~s/b//g;
		foreach my $vect (0..$#{$original_data{$indb}} ){
		 print OUTA " $original_data{$indb}[$vect] ";
		}    
    print OUTA "\n";
    foreach my $z ( @{$family{$_[1]}} ){
	if( ($data{$z}[0] ne $_[0] ) && ($data{$z}[1] ne $_[0])){
	    if ($status{$z} ne "TRIM"){ 	
		print OUTA "$_[1] $z ";
		foreach my $d (0..2){
		    print OUTA  " $data{$z}[$d] "; }
            		## then attach original phenotype and genotype
			my $tempz=$z;
			$tempz=~s/b//g;
           		 foreach my $vect (0..$#{$original_data{$tempz}} ){
               		 print OUTA " $original_data{$tempz}[$vect] ";
			}


		print OUTA "\n";
	    }else{ #if you are trimming a parent, be sure you can trim both
		if ($status{$spouse{$z}} ne "TRIM"){
		    print OUTA "$_[1] $z ";
		    foreach my $d (0..2){
			print OUTA  " $data{$z}[$d] "; }
		    	 ## then attach original phenotype and genotype
		    	 my   $tempz=$z;
				$tempz=~s/b//g;
		    	     foreach my $d (0..$#{$original_data{$tempz}} ){
		    	      print OUTA " $original_data{$tempz}[$d] ";
		    	       }
		    	 print OUTA "\n";
		}
		
	    }

	}elsif(($data{$z}[0] eq $_[0] ) ){
	    ##if the father is the individual you are duplicating
	    print OUTA "$_[1] $z ".$_[0]."b";
	    foreach my $d (1..2){
		print OUTA " ${$data{$z}}[$d]";}  
		## then attach original phenotype and genotype
		my $tempz=$z;
		$tempz=~s/b//g;
                    foreach my $d (0..$#{$original_data{$tempz}} ){
                         print OUTA " $original_data{$tempz}[$d] ";
                     }

	    print OUTA "\n";
	}elsif(($data{$z}[1] eq $_[0] ) ){
	    ## if the mother is the individual duplicated
	    print OUTA "$_[1] $z ${$data{$z}}[0] ".$_[0]."b ${$data{$z}}[2]";
		## then attach original phenotype and genotype
			my  $tempz=$z;
			$tempz=~s/b//g;
                         foreach my $d (0..$#{$original_data{$tempz}} ){
                           print OUTA " $original_data{$tempz}[$d]";
                               }
	    print OUTA "\n";
	}
	
    }
    
}


##################################

#######  read file ## argument (file_to_read)

sub readfile {
   
    open (INR, " < $_[0]") || die "pedstats had some problem, please check files names and read pedstats.out"."\n";
    my %h;
    while (my $line=<INR>){
	chomp ($line);
	my @v=split(" ",$line);
	if ($v[0] eq "") {shift @v};
	if($v[0] eq " "){shift @v;}
	undef $d{$v[1]};

	
	if(! defined  $h{$v[0]} ){$h{$v[0]}=1;}
	
	push @{$f{$v[0]}}, $v[1];  
	foreach my $i (2..$#v){  ## 2 > fath ,  3 > moth, 4 >  sex and all other informations 
	    push @{$d{$v[1]}}, $v[$i];}	## save information  for each person

	 }
    
        
    close(INR);	
    my @nf=keys %h;
   ## if you read only one family ID    
    if ($#nf <1){$status{$_[1]}='loop';
		 print "\n"."WARNING: duplicating individual $_[1] doesn't break the family. Please check for any loop. The program will ignore it."."\n";}
    
	return @nf;

}

########### arguments  (pedstats.ped, file_output)  
sub writefile {
    my $flag=0;
    if ($_[1] ne "$root_out.broken_ped"){
	$flag++;
	open (OUTAA, ">> $_[1]") || die "Impossible to open $_[1]";
    }
    open (INB, "< $_[0]") || die " Impossible to open $_[0]";
    while ( my $line=<INB>){
	if (($flag>0) && ($line =~ /[0-9]/)){
	     $line =~ s/ 1_/1_/g;
	     $line =~s/  / /g;	
		print OUTAA $line; ##if it need to be breaked again, just copy the files on to_repeat.ped
	}else{
	    if($line eq ""){next};
	    chomp($line);
	    my @v=split (" ",$line);
	    if(($v[0] eq " ")|| ($v[0] eq "")){shift @v;}
	    print OUT "$v[0] $v[1] $v[2] $v[3] $v[4] ";
	    $v[1] =~ s/ //g;
	    $v[1]=~s/b//g;
	    foreach my $d (0..$#{$original_data{$v[1]}} ){
		print OUT ${$original_data{$v[1]}}[$d]." ";
		
	    }
	    print OUT "\n";
	}
	
    }

    if ($flag>0){close (OUTAA);}
    close(INB);

}

############# arguments (two numbers)

sub diff_absol {
    if ($_[0] >= $_[1] ){ $_[0] - $_[1];
    }else{
	$_[1] - $_[0];}
    
}	

#####
